use notify_rust::Notification;
use notify_rust::Timeout;
use notify_rust::Urgency;

pub fn notify(title: &str, msg: &str, timeout: u32) {
	Notification::new()
	.summary(title)
	.body(msg)
	.timeout(Timeout::Milliseconds(timeout))
	.urgency(Urgency::Normal)
	.show()
	.expect("notification fails");
}