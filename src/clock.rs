mod centralPanel;
mod bottomPanel;

use std::time::Duration;

pub struct Clock {
	timer: crate::Timer,
	timerHour: i64,
	timerMinute: i64,
	timerSecond: u8,
	timerRunning: bool,
	inputBuffer: String,
	takingInput: i8, // -1 nothing | 0 clock | 1 minute | 2 second  / i don't want to make a enum for this
}

impl Clock {
pub fn new (context: &eframe::CreationContext<'_>) -> Self {
	let new = Self{
		timer: crate::Timer::default(),
		timerHour: 0,
		timerMinute: 0,
		timerSecond: 0,
		timerRunning: false,
		inputBuffer: String::new(),
		takingInput: -1,
	};
	context.egui_ctx.set_pixels_per_point(crate::config().pixelsPerPoint);
	new
}

pub fn timerStart(&mut self) {
	self.timer.set(Duration::from_secs(0));
	let duration = Duration::from_secs((self.timerSecond as u64) + (self.timerMinute * 60) as u64 + ((self.timerHour * 60) * 60) as u64);
	self.timer.set(duration);
	self.timerRunning = true;
}
}

pub struct Config {
	pub panel: Panel,
	pub pixelsPerPoint: f32
}

#[derive(PartialEq, Debug)]
pub enum Panel {
	clock,
	timer
}

// https://stackoverflow.com/questions/32710187/how-do-i-get-an-enum-as-a-string
use std::fmt;
impl fmt::Display for Panel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
        // or, alternatively:
        // fmt::Debug::fmt(self, f)
    }
}

impl eframe::App for super::Clock {
fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
	self.bottomPanel(ctx);
	self.centralPanel(ctx);
	if self.timerRunning {
		self.timer.update();
		
		let time = self.timer.left().as_secs();
		let mut time: chrono::Duration = chrono::Duration::seconds( match time.try_into() {
			Ok(v) => v,
			Err(..) => i64::MAX
		});
		
		self.timerHour = time.num_hours();
		time = time.checked_sub(&chrono::Duration::hours(time.num_hours())).unwrap();
		
		self.timerMinute = time.num_minutes();
		time = time.checked_sub(&chrono::Duration::minutes(time.num_minutes())).unwrap();
		
		self.timerSecond = if time.num_seconds() >= 60 {
			0
		} else {
			time.num_seconds() as u8
		};
		
		if self.timerMinute >= 60 {self.timerMinute = 0;}
		
		if self.timer.done() {
			crate::notify("Lex-Clock Timer", "DONE, timesup", 0);
			self.timerRunning = false;
		}
	}
	ctx.request_repaint_after(std::time::Duration::from_millis(200));
}
}