// Acceptable heresy
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(unused_parens)]
/*
	a simple clock and timer
	Copyright (C) 2022  Alexevier
	
	This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
mod clock;
pub mod timer;
pub mod notify;

use clock::*;
pub use timer::*;
pub use notify::*;

pub static mut CONFIG: clock::Config = clock::Config{
	panel: clock::Panel::clock,
	pixelsPerPoint: 1.
};

pub fn config() -> &'static mut clock::Config {
	unsafe { &mut CONFIG }
}

fn main() {
//init
	let mut options = eframe::NativeOptions::default();
	options.resizable = true;
	options.transparent = true;
	options.vsync = true;
	options.initial_window_size = Some(egui::Vec2::new(350.,205.));
//run
	eframe::run_native("Lex-Clock", options, Box::new(|context| Box::new(Clock::new(context))));
}