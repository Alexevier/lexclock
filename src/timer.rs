// might be moved to Lex-Lib
use std::time::{Duration, SystemTime};

pub struct Timer {
	time_left: Duration,
	time_elapse: SystemTime,
}

impl Timer {
	pub fn set(&mut self, duration: Duration){
		self.time_left = duration;
	}
	
	pub fn update(&mut self){
		// this fn is a mess
		if !self.time_left.is_zero() {
			self.time_left = match self.time_left.checked_sub(
				match self.time_elapse.elapsed() {
					Ok(t) => t,
					Err(..) => panic!("did system time change?")
				}
			) {
				Some(t) => t,
				None => Duration::from_secs(0)
			}
		}
		self.time_elapse = SystemTime::now();
	}
	
	pub fn restart(&mut self) {
		self.time_elapse = SystemTime::now();
		self.update();
	}
	pub fn done(&self) -> bool {
		self.time_left.is_zero()
	}
	pub fn left(&self) -> Duration {
		self.time_left
	}
}

impl Default for Timer {
	fn default() -> Self {
		Self {
			time_left: Duration::from_secs(0),
			time_elapse: SystemTime::now(),
		}
	}
}

impl std::ops::Add<Duration> for Timer {
	type Output = Self;
	fn add(self, rhs: Duration) -> Self::Output {
		Self {
			time_left: self.time_left + rhs,
			time_elapse: self.time_elapse
		}
	}
}