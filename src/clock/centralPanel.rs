use chrono;
use crate::config;

impl super::Clock {
pub fn centralPanel(&mut self, ctx: &egui::Context){
	egui::CentralPanel::default().show(ctx, |ui|{
	ui.vertical_centered_justified(|ui|{
		ui.label(egui::RichText::new(config().panel.to_string()).weak().size(35.));
		if config().panel == super::Panel::timer {self.timerPanel(ui);}
		if config().panel == super::Panel::clock {self.clockPanel(ui);}
	})});
}

fn clockPanel(&mut self, ui: &mut egui::Ui){
	ui.label(egui::RichText::new(chrono::Local::now().time().to_string().split('.').collect::<Vec<&str>>()[0]).size(75.));
	ui.label(egui::RichText::new("utc: ".to_owned() + chrono::Utc::now().time().to_string().split('.').collect::<Vec<&str>>()[0]).size(20.));
}

fn timerPanel(&mut self, ui: &mut egui::Ui){
ui.horizontal_centered(|ui|{

	let mut n = self.timerHour.to_string();
	if n.len() == 1 {n.insert(0, '0')}
	if ui.add(egui::Label::new(egui::RichText::new(n).size(75.)).sense(egui::Sense::click())).clicked(){
		self.takingInput = 0;
	}
	
	ui.label(egui::RichText::new(":").size(75.));
	
	let mut n = self.timerMinute.to_string();
	if n.len() == 1 {n.insert(0, '0')}
	if ui.add(egui::Label::new(egui::RichText::new(n).size(75.)).sense(egui::Sense::click())).clicked(){
		self.takingInput = 1;
	}
	
	ui.label(egui::RichText::new(":").size(75.));
	
	let mut n = self.timerSecond.to_string();
	if n.len() == 1 {n.insert(0, '0')}
	if ui.add(egui::Label::new(egui::RichText::new(n).size(75.)).sense(egui::Sense::click())).clicked(){
		self.takingInput = 2;
	}
});
}

}