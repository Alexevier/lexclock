use crate::config;

impl super::Clock {
pub fn bottomPanel(&mut self, ctx: &egui::Context){
egui::TopBottomPanel::bottom("bottom panel").min_height(35.).resizable(false).show(ctx, |ui|{
ui.horizontal_centered(|ui|{
{ //change from clock to time and viceversa
	if config().panel == super::Panel::clock {
		if ui.add(egui::Label::new(egui::RichText::new("timer").size(20.)).sense(egui::Sense::click())).clicked(){
			config().panel = super::Panel::timer;
		}
	}
	else {
		// input when changing timer
		if self.takingInput != -1 {
			if ui.text_edit_singleline(&mut self.inputBuffer).lost_focus() {
				match self.takingInput {
					-1 => println!("clock.takingInput: -1"),
					0 => { self.timerHour = match self.inputBuffer.trim().parse() {
						Ok(v) => v,
						Err(..) => 0
					}},
					1 => { self.timerMinute = match self.inputBuffer.trim().parse() {
						Ok(v) => v,
						Err(..) => 0
					}},
					2 => { self.timerSecond = match self.inputBuffer.trim().parse() {
						Ok(v) => v,
						Err(..) => 0
					}},
					_ => { panic!("clock.takingInput is invalid, should be -1 to 2") }
				}
				self.inputBuffer.clear();
				self.takingInput = -1;
			}
		}
		
		if ui.add(egui::Label::new(egui::RichText::new("clock").size(20.)).sense(egui::Sense::click())).clicked(){
			config().panel = super::Panel::clock;
		}
		ui.label(" | ");
		if self.timerRunning {
			if ui.add(egui::Label::new(egui::RichText::new("pause").size(20.)).sense(egui::Sense::click())).clicked(){
				self.timerRunning = false;
			}
		} else {
			if ui.add(egui::Label::new(egui::RichText::new("start").size(20.)).sense(egui::Sense::click())).clicked(){
				self.timerStart();
				self.timer.restart();
			}
		}
	}
}
})});
}}